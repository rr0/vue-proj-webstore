# vue_proj
General webstore front-end for mobile and desktop. Quick and easy to manage and deploy. 
## Example of a local instance
![home_demo](https://user-images.githubusercontent.com/5626537/116797326-6ec42a80-aa99-11eb-82a8-9dce6d1e2e98.png)
![product_demo](https://user-images.githubusercontent.com/5626537/116797327-72f04800-aa99-11eb-8069-fa360d9eada2.png)
![checkout_demo](https://user-images.githubusercontent.com/5626537/116797333-7aafec80-aa99-11eb-9bab-6e0e2ab34cc4.png)
