import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

import Product from '../views/Product.vue'
import Category from '../views/Category.vue'
import Search from '../views/Search.vue'
import Cart from '../views/Cart.vue'
import SignUp from '../views/SignUp.vue'
import LogIn from '../views/LogIn.vue'
import Account from '../views/Account.vue'
import Checkout from '../views/Checkout.vue'
import Success from '../views/Success.vue'
import All from '../views/All.vue'

// Create paths after you've defined them in views.
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/all',
    name: 'All',
    component: All,
  },
  {
    path: '/:category_slug/:product_slug',
    name: 'Product',
    component: Product,
  },
  {
    path: '/:category_slug/',
    name: 'Category',
    component: Category,
  },
  {
    path: '/search',
    name: 'Search',
    component: Search,
  },
  {
    path: '/bag',
    name: 'Cart',
    component: Cart,
  },
  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp,
  },
  {
    path: '/log-in',
    name: 'LogIn',
    component: LogIn,
  },
  {
    path: '/account',
    name: 'Account',
    component: Account,
    meta: {
        requireLogin: true,
    },
  },
  {
    path: '/bag/checkout',
    name: 'Checkout',
    component: Checkout,
    meta: {
        requireLogin: true,
    },
  },
  {
    path: '/bag/checkout/success',
    name: 'Success',
    component: Success,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireLogin) && !store.state.isAuthenticated) {
    next({ name: 'LogIn', query: {to: to.path } });
  } else {
    next()
  }
})

export default router
